package com.example.testmongo.grocery;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(path = "api/grocery")
@EnableMongoRepositories

public class GroceryController {

    private final GroceryService groceryService;

    @Autowired
    public GroceryController(GroceryService groceryService) {
        this.groceryService = groceryService;
    }

    @GetMapping
    public List<GroceryItem> getAll() {
        return this.groceryService.getAll();
    }

    @PostMapping()
    GroceryItem save(@RequestBody GroceryItem newGroceryItem) {
        return groceryService.save(newGroceryItem);
    }
}
