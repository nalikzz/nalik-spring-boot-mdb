package com.example.testmongo.grocery;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.example.testmongo.mdbspringboot.ItemRepository;

@Service
public class GroceryService {
    
    @Autowired
	ItemRepository groceryItemRepo;

    @GetMapping
    public List<GroceryItem> getAll() {
		return groceryItemRepo.findAll();
	}

	@PostMapping
	public GroceryItem save(@RequestBody GroceryItem newGroceryItem){
		return groceryItemRepo.save(newGroceryItem);
	}

}
