package com.example.testmongo.mdbspringboot;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.testmongo.grocery.GroceryItem;

public interface ItemRepository extends MongoRepository<GroceryItem, String> {

}